<?php

namespace yii\securitychecker\exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
