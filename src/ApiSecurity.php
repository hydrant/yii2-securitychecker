<?php

namespace yii\securitychecker;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use yii\web\Request;

class ApiSecurity {

  public function authenticateApiRequest(Request $request) {
    // String containing the token or Null.
    $token = $request->headers->get('Authorization');

    if (!isset($token)) {
      return FALSE;
    }

    // Check that the auth type is token.
    if (substr($token, 0, 6) !== 'Bearer') {
      return FALSE;
    }

    // Remove prefix once we have checked that it is present so that the token
    // can then be parsed.
    $token = substr($token, 7);

    $signer = new Sha256();

    $key = getenv('MONITOR_PUBLIC_KEY');
    $publicKey = new Key($key);
    $parser = new Parser();

    // Make sure we can parse the token correctly.
    try {
      $parsed_token = $parser->parse($token);
    }
    catch (\Exception $e) {
      return FALSE;
    }

    // Check the token signture againt the public key.
    if (!$parsed_token->verify($signer, $publicKey)) {
      return FALSE;
    }

    // Grab the tokens expiration value.
    $exp = $parsed_token->getClaim('exp');

    // Check the token has not expired.
    if ($exp < time()) {
      return FALSE;
    }

    return TRUE;
  }

}

