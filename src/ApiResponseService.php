<?php

namespace yii\securitychecker;

use yii\securitychecker\exception\ExceptionInterface;
use yii\securitychecker\SecurityChecker;

class ApiResponseService {

  /**
   * Build the agent data json format.
   *
   * @return array
   */
  function buildResponse() {
    return [
      'type' => 'agent-data',
      'version' => '2',
      'data' => [
        'version' => '1.0.0',
        'platform' => [
          'name' => 'Yii',
          'version' => \Yii::getVersion(),
        ],
        'language' => [
          'name' => 'PHP',
          'version' => phpversion(),
        ],
        'components' =>
          $this->formatVulnerabilities(),
      ],
      'extended_data' => [],
    ];
  }

  /**
   * Make query to Symfony security checker with the projects
   * composer.lock file.
   *
   * @return array|\yii\securitychecker\Result
   */
  function queryForInsecureComponents() {
    $checker = new SecurityChecker();

    $path = \Yii::getAlias('@root') . '/composer.lock';

    try {
      $result = $checker->check($path, 'json');
    } catch (ExceptionInterface $e) {
      // @ToDo log this.
    }

    return $result ? $result : [];
  }

  /**
   * Formats the data returned from Symfony checker into
   * an array.
   *
   * @return array
   */
  function formatVulnerabilities() {
    if ($this->queryForInsecureComponents() === []) {
      return [];
    }

    $json_vulns = $this->queryForInsecureComponents()->__toString();
    $decoded_vulns = json_decode($json_vulns);

    $vulns = [];
    $id = 0;
    foreach ($decoded_vulns as $name => $info) {
      $vulns[$id]['type'] = 'php-package';
      $vulns[$id]['name'] = $name;
      $vulns[$id]['version'] = $info->version;
      $id++;
    }

    return $vulns;
  }

}
