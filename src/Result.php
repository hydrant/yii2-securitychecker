<?php

namespace yii\securitychecker;

class Result implements \Countable
{
  private $count;
  private $vulnerabilities;
  private $format;

  public function __construct($count, $vulnerabilities, $format)
  {
    $this->count = $count;
    $this->vulnerabilities = $vulnerabilities;
    $this->format = $format;
  }

  public function getFormat()
  {
    return $this->format;
  }

  public function __toString()
  {
    return $this->vulnerabilities;
  }

  public function count()
  {
    return $this->count;
  }
}
