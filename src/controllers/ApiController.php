<?php

namespace yii\securitychecker\controllers;

use yii\caching\DummyCache;
use yii\helpers\VarDumper;
use yii\securitychecker\ApiResponseService;
use yii\securitychecker\ApiSecurity;
use yii\securitychecker\DataEncryptionService;
use yii\securitychecker\exception\ExceptionInterface;
use yii\securitychecker\SecurityChecker;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;

class ApiController extends Controller {

  /**
   * @var \yii\securitychecker\ApiResponseService
   */
  private $apiResponseService;

  /**
   * @var \yii\securitychecker\ApiSecurity
   */
  private $security;

  /**
   * @var \yii\securitychecker\DataEncryptionService
   */
  private $dataEncryptionService;

  /**
   * ApiController constructor.
   *
   * @param $id
   * @param $module
   * @param \yii\securitychecker\ApiResponseService $apiResponseService
   * @param \yii\securitychecker\ApiSecurity $security
   * @param \yii\securitychecker\DataEncryptionService $dataEncryptionService
   * @param array $config
   */
  public function __construct($id, $module, ApiResponseService $apiResponseService, ApiSecurity $security, DataEncryptionService $dataEncryptionService, $config = []) {
    parent::__construct($id, $module, $config);

    $this->apiResponseService = $apiResponseService;
    $this->security = $security;
    $this->dataEncryptionService = $dataEncryptionService;
  }

  /**
   * @return string
   * @throws \yii\web\ForbiddenHttpException
   */
  public function actionIndex()
  {
    if ($this->security->authenticateApiRequest(\Yii::$app->request) === FALSE) {
      throw new ForbiddenHttpException('You are not authorised to view this page.');
    }

    $data = json_encode($this->apiResponseService->buildResponse());
    $fileSaved = file_put_contents(\Yii::getAlias('@root') . '/json_data.txt', $data);

    $encryptedData = $this->dataEncryptionService->formatEncryptedData();

    unlink(\Yii::getAlias('@root') . '/json_data.txt');
    unlink(\Yii::getAlias('@root') . '/enc.txt');

    return $encryptedData;
  }

}
