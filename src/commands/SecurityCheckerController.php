<?php

namespace yii\securitychecker\commands;

use yii\console\Controller;
use yii\securitychecker\exception\ExceptionInterface;
use yii\securitychecker\SecurityChecker;

class SecurityCheckerController extends Controller
{
  public $format = 'text';
  public $endpoint;
  public $timeout;
  public $token;

  private $checker;

  public function __construct($id, $module, SecurityChecker $checker, $config = [])
  {
    $this->checker = $checker;

    parent::__construct($id, $module, $config);
  }

  public function options($actionID)
  {
    return [
      'format',
      'endpoint',
      'timeout',
      'token',
    ];
  }

  public function actionIndex($lockfile)
  {
    if ($this->endpoint) {
      $this->checker->getCrawler()->setEndPoint($this->endpoint);
    }

    if ($this->timeout) {
      $this->checker->getCrawler()->setTimeout($this->timeout);
    }

    if ($this->token) {
      $this->checker->getCrawler()->setToken($this->token);
    }

    $format = $this->format;

    try {
      $result = $this->checker->check($lockfile, $format);
    } catch (ExceptionInterface $e) {
      $this->stdout($e->getMessage(), Console::FG_RED);
      return 1;
    }

    $this->stdout((string) $result);

    if (\count($result) > 0) {
      return 1;
    }

    return 0;
  }

}
