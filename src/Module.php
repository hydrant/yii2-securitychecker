<?php

namespace yii\securitychecker;

use Yii;
use yii\console\Application;

/**
 * securitychecker module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'yii\securitychecker\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
      parent::init();

      if (Yii::$app instanceof Application) {
        $this->controllerNamespace = 'yii\securitychecker\commands';
      }
    }
}
