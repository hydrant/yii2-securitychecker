<?php

namespace yii\securitychecker;

class DataEncryptionService {

  /**
   * Gets a public cert from an env variable.
   *
   * @return resource
   *   The public certificate.
   */
  public function getCert() {
    $cert = getenv('MONITOR_CERTIFICATE');
    
    // Public Cert.
    return openssl_x509_read($cert);
  }

  /**
   * Uses openssl and file paths to encrypt data.
   *
   * @return bool
   *   Boolean based on result of openssl encrypt function.
   */
  public function encryptData():bool {
    $in = \Yii::getAlias('@root') . '/json_data.txt';
    $out = \Yii::getAlias('@root') . '/enc.txt';
    $public_cert = $this->getCert();

    $encryption_status = openssl_pkcs7_encrypt($in, $out, $public_cert, [], 0, OPENSSL_CIPHER_AES_256_CBC);

    if (!$encryption_status) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Pulls and formats data from enc.txt.
   *
   * @return string
   *   Formatted string containing the encrypted data.
   */
  public function formatEncryptedData():string {
    if ($this->encryptData() === FALSE) {
      return '';
    }

    $data = file_get_contents(\Yii::getAlias('@root') . '/enc.txt');
    if (empty($data)) {
      return '';
    }

    // Get the encrypted body without the headers.
    $parts = explode("\n\n", $data, 2);

    $prefix = "-----BEGIN PKCS7-----\n";
    $suffix = "\n-----END PKCS7-----\n\n";

    // Adding necessary prefix/suffix.
    $encrypted_data = $prefix . rtrim($parts[1]) . $suffix;
    return $encrypted_data;
  }

}
