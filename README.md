# YII2 Security Checker
### An initial port of the SensioLabs Symfony bundle over to YII2.
https://github.com/sensiolabs/security-checker

---

# Installation

Via Composer

```
composer require upbeat/yii2-security-checker
```

Then add the below code to your common/config main.php config file:

```
'modules' => [
  'securitychecker' => [
    'class' => 'yii\securitychecker\Module',
  ],
]
```

Add this to common/config/bootstrap.php
```
Yii::setAlias('@root', realpath(dirname(__FILE__).'/../../'));
```

In frontend/config/main.php add this rule
```
            'rules' => [
                [
                  'pattern' => 'upbeat-status-monitor/agent-data',
                  'route' => 'securitychecker/api',
                  'suffix' => '.json',
                ],
              ];
```


You can then run the command with

```
yii securitychecker/security-checker path/to/lock/file
```

